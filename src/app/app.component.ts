import { Component, OnInit } from '@angular/core';
import { AngularFire, AuthProviders } from 'angularfire2';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeesService } from './services/employees.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [ EmployeesService ]
})

export class AppComponent implements OnInit {

  LoginImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUAAAACECAMAAADWQAKOAAAAzFBMVEX////zLgbxKQD1JwDu6efYZE3iemzfKwnZKgnvHQDhtav0GgDoycH//f3zLgD/+fn+8vD3i3/7yMP95eP3dl/0QyT3d2T0PB7zNA71WD/5oJX3hHb6u7T2ZVP93tr0NwD1Vzj+7uz4lIn6rKD7wrv0Rir3e2z82tb0OxT80Mn6tKv2bVv4jHj1X0j829f4e2L5o5X4mYn6tqn2ZE36rqH1Uzz3gGz1TDHgQyXg19XWORHMlYvkPBzQo5vVsqvYh3rWeWzMYk/dwLvWRihsYO86AAAJtklEQVR4nO2ba5uiOBaAQ9K7ta5LEqG4KEoREQGRGi1UdmZ3u3t6/v9/2hO8lD2t1WBV7/PU9nk/VAmE20uScxKUEARBEARBEARBEARBEARBEARBEARBEARBEARBEARBEARBEARBEARBEAT5P0N8/PyXjvz+e9c93iOfvohWAv/1b8o6cnfXdY/3CP3nf9r4+/U3ZnTl7m+dd3mPsD8+tBD4pdf9yD+JQN77RwuBf+93P/JPItBAga8EBb4SFPhKbhXI6Rn8woFR4EsCKc2S7fDINrtgEAVeF8hZEsSzNDxgLR0U2EUg5ZPczse5hC1mmM92yaUDo8BrAmkSm2nV7/dLyyTmLOkzeunAKPCKQJ6thJhoZ7RMoQou+KUQggKvCmSuInm/kUYDkxBRXB4no8DLAjmfEbLZL9MshI0brIFdBLKhIOboMLnQX8HGPME+sItAF5bjwzLbQSR+jN5I4JXO9LT1xc1f7//dsrdexbd0bsIDWLaOAkv7XCA9j8etBeqhjP7/8PDw0rXD5pePw88KfHuoawOmC0fpZrCrQBrAsjo8JrqFTjB3jtqiqn4+eUuBjCZR5PSYwdZhWl2dt+VlbsfeS7dG3TAcMK4fInWscO2fF4aRUxQlnH3XDZ+nadDqwo90bsKVXrHcO2sErg53zbOcPD0baCmwWthS2qshZysVXgno+kxVql4WyOa2mrIsKhNOE5tszgXSZJ1Kqazg+93AQJjTTlWwcw10dOS19rVON2FVvkIgjGmITB8lsYZsW0yaISHcJNetTTfs0w1zb1IMfUM3wysSeFIUNSsttebUsUnsP7dYzjfCtHNF5LxZx597mlMh3pyRg0DRCOStGrxxSx44FZD8rX19Eb1AyNHxam4QyLORKYJtOSJiQFmvp5VRL6NZ4hl+4vjM8473QXs9xnmS+CzzjgopgzZ5+qNLsCqEFsG0QM4derg05gpzPNkGjyTP4AEw/9iYKXW8fSHmO5Q7iX8QeFbmzQVSZwHpsz3gcLlOKuJDXWSszy3y1NdvqtoLdMbEhpvPrHTKd0+bIdzR2hrPB/nIq6xxEVjWYD/Tw7erpyDjuTV3Z9Zof07YY+3Q+mnpU/dplVSbp/tyJkkYlyBwcw/l9gkW52MSVowZI2knlDmDWf40zyisLzeWFdf60/3CGk0WVsUagSxbzvKF67cxeMtYeAWxV8yWu5Ut9zURurIlEJJc/ytpa4H+ipjr2mfQdnobIu5ZDWm6UAr6CJeYthTHLpZOFBl7HJ6cgnXjJlLyKVGT3og8lmwM2ShEt9EvkFaZ9hwEKl3OauoqrVMy9illXln6NEmJKYiIPepPFRGChAVnAXyStiD3e4E+XIUkctrG4C2zMb47zkNbT8aMs/0zNjZnpUesrUDwkhORj1xoP72YSLh+ScbzWGqBppkOBiEJm7ugE/sgcLSDbnOoT8snUgbUImrOUmiygSCjbADFKgcE2uu5RcwmwzoIdOoaegAjJip2Z6YMeuUjSYNpSKyap1A+yIl5EDggMr63SFi2eJt703wg9ZPhNocNu+MZ1mEYwhOUen5w2V6gYUwWiphqXO0F+guiaualTQ2UEOth5V7CQaC58elakIk+BXVScwRhzBwlSgRMC+yf+sCNzwJJ5uwkkMW5Zc2q5JGMk14ZkrHuzF1GB0S5MLqKfVg+CHywSFrxpRC7HyRQdyu0r6Px9Jg6O9vtdpiTcQ3/99Or7QRSlhWr1CR52dcCsxnJHyjP9wJ3lK+IaKrBSWDs86kkv+h1HHSPp1Kas7mwi14jcAIC6T4Ks/lXAnsWdAjSjZQ5gl4IqtcDnDCjLCJiGhB4AHQgDwIzG1KDPDTN5Q8TCPR1DVTBoQ/k0MX4EIV7p4S/lUB/O0xYj++gBnAt0AN1/CQweFmgwe5JmJrL3LZMy2PXBSa57mu8bC2kO5QCBOrgBQIV9IyJKdYBJDiMPwuEHlHZth3+uBoIxvpT3QmqqfeKNIZuLbVglHGwkx1qYPrA2gqkkRBCDWMhzYV/XSDXRx0y2oeky41gU8bqnOQcUqiIsQLO40IadVYDHyAaFtvhdvti5v4KgTBe4s4wsPUmtTrUwZsEJjNiF1kGt7ppaiCPTVE8gKxWAjnXI0nd1+k08ihQx4uvBBoMmuhiq2fSpatlFuAKQp2rSOzVkOKUiSSz2ovNo8CFTtP8avJjojBlD2VspRCGTb1NTA9tlmdPatV1KAf5g8phjKDm0AdCGuNC/Mxt86pAogWKg0CjD+nGWkcEe8JYYIJACG1yNvmTQJpZRIRWCjnikLkm0YOStGTJmIg0FSZUAf0pl6coXDZXZaflxXmm1wnktF6HRFmj3aS6n+kvx4m1cTDY75/1Ge2GcrQaKyGkVVAYC9sFg8E8LOkaWCgbgsha2cNGIIyFNx4XauTzpQ2mm93ZVIESvpAzSJDnSk371H0UkAemcuXDw5DuvhzNpqmERG8BGXh/l0uhZvBYaDKCc0Ndg45woYSd6ygc2LCiX1iwJRzVby+Qw+hVqEXpcQbhwokVbL38oFqmMTQr5/MKOhvuRFHGOa/dYpiSmZNFkQfRHVY25bI6SgweRY5heLrg/mq8qPZhzAab9OcI4j/fzgvHqKEcz07l4LLrYl7UTdbKncm8bHo37g/nbqSTbZ5N5tFamAXzmtNyr4LiP2IkQouUyJFzyF6oB8m8bmq3C9Sjdt6MR5v4Td1BFK2kHso24Zwfx7O8mTVs/vDnmb3TnsZpNdXTAH8u13w+LjYfn8+tZxG2Azdyc6KiY6mz4m8qkNYWDD9OE4AwKFlDDz4y3urFOvdgjAB9a9im93k7IJ4IyBLNuPv3SDsK5EsYIgZnp6EQ1XRk/PbANwoMLNtOF/UNd/IK6HCR2qF1TGo70Ukg92Nose753bHVW9ZA3RuV5db4n9Y/fVajLkvvpjcp3QRmT+TrGmiwtR5yXTjwrW/l9KT87a+EbkWf9ban1rEGgkAzPpvRNVhM0u1bCnxvdOwD9fjN3p0MclaFMrhYYVDgJYF0CFGYhANf13gKI7rhTKwufTsQBV4WaPBCT8LIRVDWnleX0xzGB5f7DhR4WSAvcz0ElnZuWamSaXDFHwq8LBCixoO7yG0lpbL1e5fLXw40UOBVgRA4aFJWRVENHfbCmz8UeE2goUeKzQ/tXkycUOALAtuAAlFgK1DgK2kl8CP+3PUqrQT+eoc/uL4MZ7+1+cG1/sl/V+7uOu/yDjHa/eSffPj46a8d+fy56x7vkU9fWlVAQswPyEXMdv4QBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEF+dv4LFTQ3MxhFwD0AAAAASUVORK5CYII=";
  constructor(public angularFire: AngularFire,
              private router: Router,
              public _employeesService: EmployeesService){
    angularFire.auth.subscribe(user => {
      if(user) {
        // user logged in
        console.log("logged in");
        console.log(user);
        this._employeesService.getAllEmployees()
         .subscribe( data => {
           var companyUser = false;
           var UserEmail = (user.auth.email).toLowerCase();
           for(var key in data){
             var employeeEmail = (data[`${key}`]['email_id']).toLowerCase();
             if(employeeEmail == UserEmail){
                companyUser = true;
             }
           }
           if(companyUser == true){
             this.router.navigate(["/list-of-peers-to-rate"]);
           }
           else{
             
           }
         });
      }
      else {
        // user not logged in
        console.log('not logged in');
        this.router.navigate(["home"]);

      }
    });
  }

  login() {
    this.angularFire.auth.login({
    provider: AuthProviders.Google
    });
  }

  logout() {
    this.angularFire.auth.logout();
  }

  ngOnInit(){
    this.getAllPeers();
  }

  getAllPeers(){
    //this.allEmployeesEligibleForRating = this._employeesProjectsService.getAllPeers();
    //console.log(this.allEmployeesEligibleForRating);
  }
}
