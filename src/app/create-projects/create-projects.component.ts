import { Component, OnInit } from '@angular/core';
import { ProjectsService } from './../services/projects.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFire, AuthProviders } from 'angularfire2';


@Component({
  selector: "create-projects",
  templateUrl: "./create-projects.component.html",
  styleUrls: ["./create-projects.component.scss"],
  providers: [ ProjectsService ]
})

export class CreateProjectsComponent implements OnInit{
  Months = ['January', 'February', 'March','April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  month: any;
  CsdProjectKey: any = "";
  UserEmail: any;
  ProjectsList = [];
  Projects = [];
  listDisplay1: any = false;
  listDisplay2: any = false;
  displayValue1 : any;
  displayValue2 : any;
  project: any;
  Client: any;
  Clients = [];
  Location: any;
  Locations = [];

  constructor(public _projectsService: ProjectsService,
              private route: ActivatedRoute,
              public angularFire: AngularFire,
              public router: Router) {
               this.month = new Date().getMonth();
               this.month = this.Months[this.month]; 

               this.route.queryParams.subscribe( params => {
                    this.CsdProjectKey = params["CsdProjectKey"];
               });  
               alert(this.CsdProjectKey);
               angularFire.auth.subscribe(user => {
                  if(!user) {
                    // user logged out
                    this.router.navigate(["home"]);
                  }
                  else{
                    this.UserEmail = user.auth.email;
                  }
               });

              }


  createProject(){
      var project = (document.getElementsByTagName("input")[0]).value;
      var projectType = ((document.getElementsByTagName("input")[2]).checked) ? 'Internal' : 'External' ;
      var newProject = {
         'project_name' : project,
         'client_id' : this.Client,
         'project_type': projectType,
         'location' : this.Location
      }
      if(project==""){
        alert('Please Enter Project Name');
      }
      else if(this.Client =="Select Client"){
        alert('Please Enter Client Name');
      }
      else if (this.Location == "Select Location"){
        alert('Please Enter Location');
      }
      else{
          if((this.CsdProjectKey == "") || (this.CsdProjectKey == undefined)){
              this._projectsService.createProject(newProject);
          }
          else{
              this._projectsService.updateCsdProject(newProject,this.UserEmail, this.CsdProjectKey);
          }
          this.router.navigate(["csd-projects"]);
      }
  }
  showList(val,x){

      this.Client = x.value;
      this.displayValue1 = x.label;
      this.listDisplay1 = !this.listDisplay1;
      console.log(this.Client);
      console.log(x.value);
   }
  showList2(val,x){
      this.Location = x.value;
      this.displayValue2=x.label;
      this.listDisplay2 = !this.listDisplay2;
      console.log(this.Location);
      console.log(x.value);
   }

  GoBack(){
    this.router.navigate(["list-of-peers-to-rate"]);
  }

   generateClientsList(Keys, data){
     this.Clients = [];
     for (var i=0; i < data.length; i++){
       this.Clients.push(
         {
           value: Keys[i],
           label: data[i]
         }
       );
     }      
   }

  generateLocationsList(data){
       this.Locations = [];
       for(var i=0; i < data.length; i++){
         this.Locations.push(
           {
             value: data[i],
             label: data[i]
           }
         );
       }
 }

  getClients(){
    this._projectsService.getClients()
        .subscribe( data => {this.generateClientsList(data[0], data[1]);});
  }

  getLocations(){
    this._projectsService.getLocations()
        .subscribe( data => {this.generateLocationsList(data);});
  }

  ngOnInit(){
    if(this.Client == undefined){
      this.displayValue1 = 'Select Client';
    }
    else{
      this.displayValue1 = this.Client;
    }

   if(this.Location == undefined){
      this.displayValue2 = 'Select Location';
    }
    else{
      this.displayValue2 = this.Location;
    }
    this.getClients();
    this.getLocations();
    console.log('ends');
  }

}
