import { Component, OnInit } from '@angular/core';
import { EmployeesService } from './../services/employees.service';
import { Router } from '@angular/router';
import { AngularFire, AuthProviders } from 'angularfire2';


@Component({
  selector: "appreciation",
  templateUrl: "./appreciation.component.html",
  styleUrls: ["./appreciation.component.scss"],
  providers: [ EmployeesService ]
})

export class AppreciationComponent implements OnInit{
     month: any;
     UserEmail: any;
     Months = ['January', 'February', 'March','April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
     EmployeesData = []
     IconActive: any = true;
     EmployeeKeys = [];
     EmployeeNames = [];
     EmployeeRoles = [];
     constructor(public _employeesService: EmployeesService,
              public angularFire: AngularFire,
              public router: Router) {
               this.month = new Date().getMonth();
               this.month = this.Months[this.month]; 

               
               angularFire.auth.subscribe(user => {
                  if(!user) {
                    // user logged out
                    this.router.navigate(["home"]);
                  }
                  else{
                    this.UserEmail = user.auth.email;
                  }
               });

              }
 /*   getEmployeesDetails(data, Keys){
      this._employeesService.getEmployeesDetails(data,Keys)
          .subscribe(Data => {
                        this.EmployeesData = Data[0];
                        this.EmployeeNames = Data[1];
                        this.EmployeeRoles = Data[2];
                        this.EmployeeKeys  = Data[3];
          });
}*/
    getEmployeesAppreciation(){
       this._employeesService.getEmployeesDetails(this.UserEmail)
           .subscribe(data => {
                            console.log(data[0]);
                            console.log(data[1]);
                            console.log(data[2]);
                            this.EmployeeNames = data[0];
                            this.EmployeeRoles = data[1];
                            this.EmployeeKeys = data[2];
      });   
    }


    ngOnInit(){
        this.getEmployeesAppreciation();
    }
}