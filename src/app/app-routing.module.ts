import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { CreateProjectsComponent } from './create-projects/create-projects.component';
import { CsdProjectsComponent } from './csd-projects/csd-projects.component';
import { AppreciationComponent } from './appreciation/appreciation.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { NotificationTileComponent } from './notification-tile/notification-tile.component';


/** Custom components **/

const routes : Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'notifications',
    component: NotificationsComponent
  },
  {
    path: 'appreciation',
    component: AppreciationComponent
  },
  {
    path: 'create-projects',
    component: CreateProjectsComponent
  },
  {
    path: 'csd-projects',
    component: CsdProjectsComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'user-profile',
    component: UserProfileComponent
  }

];

@NgModule({
  imports: [ RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
})

export class AppRoutingModule{}
