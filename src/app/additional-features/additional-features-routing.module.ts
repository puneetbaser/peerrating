import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';

/** Custom components **/

const appRoutes : Route = [

];

@NgModule({
  imports: [

  ],
  exports: [
    RouterModule
  ]
})

export class AdditionalFeatureRoutingModule{

}