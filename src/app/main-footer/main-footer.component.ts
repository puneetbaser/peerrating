import { Component, OnInit, Input } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { AngularFire, AuthProviders } from 'angularfire2';

@Component({
  selector: 'main-footer',
  templateUrl: './main-footer.component.html',
  styleUrls: ['./main-footer.component.scss']
})
export class MainFooterComponent implements OnInit {
  @Input() CSDLogin: any = true;
  @Input() active1: any = false;
  @Input() active2: any;
  @Input() active3: any;
  @Input() active4: any = false;
  listDisplay = false;
  display: any= false;
  createProjectsDisplay: any = false;
  rev : any = true
  Active(x){
    if(this.active4 == false){
      this.active4 = true;
    }
    else{
      this.active4 = false;
    }
  }
  appreciation(){
    console.log('coming');
    this.router.navigate(["appreciation"]);
  }
  GoHome(){
    this.router.navigate(["list-of-peers-to-rate"]);
  }
  userProfile(){
    let navigationExtras : NavigationExtras = {
          queryParams: {
               "CSDLogin": this.CSDLogin
          }
    }
    this.router.navigate(["user-profile"], navigationExtras);

  }
  Notifications(){
    this.router.navigate(['notifications']);
  }
  logout() {
    this.angularFire.auth.logout();
  }
  showList(){
    if(this.display == true){
      this.display = false;
    }
    else{
      this.display = true;
    }
    this.listDisplay =  this.display;
    if(this.CSDLogin == true){
      this.createProjectsDisplay = true;
      this.rev = false;
    }
  }

  CsdProjects(){
    this.router.navigate(['csd-projects']);
  }

  createProjects(){
    this.router.navigate(["create-projects"]);
  }

  constructor(public router: Router,
              public angularFire: AngularFire) { }

  ngOnInit() {
  }

}
