import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { InMemoryWebApiModule } from '@angular/../angular-in-memory-web-api';
import { InMemoryDataService } from './services/in-memory-data.service'
import { AngularFireModule, AuthProviders, AuthMethods } from 'angularfire2';


import { AppComponent } from './app.component';
import { MainHeaderComponent } from './main-header/main-header.component';
import { MainFooterComponent } from './main-footer/main-footer.component';
import { CreateProjectsComponent } from './create-projects/create-projects.component';
import { CsdProjectsComponent } from './csd-projects/csd-projects.component';
import { ProjectTileComponent } from './project-tile/project-tile.component';
import { AppreciationComponent } from './appreciation/appreciation.component';
import { EmployeeTileComponent } from './employee-tile/employee-tile.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { NotificationTileComponent } from './notification-tile/notification-tile.component';

import { HomeModule } from './home/home.module';
import { UserProfileModule } from './user-profile/user-profile.module';
import { AppRoutingModule } from './app-routing.module';
import { SelectModule } from './../../node_modules/angular2-select';

export const firebaseConfig = {
  apiKey: "AIzaSyCy0w1mXREQRhV74scNNJNuQ0byu5YdcA4",
  authDomain: "peerratingapp.firebaseapp.com",
  databaseURL: "https://peerratingapp.firebaseio.com",
  storageBucket: "peerratingapp.appspot.com",
  messagingSenderId: "668419528864"
};


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    InMemoryWebApiModule.forRoot(InMemoryDataService),
    CommonModule,
    HomeModule,
    UserProfileModule,
    AppRoutingModule,
    SelectModule,
    AngularFireModule.initializeApp(firebaseConfig,{
      provider: AuthProviders.Google,
      method: AuthMethods.Popup
    })
  ],
  declarations: [
    AppComponent,
    MainHeaderComponent,
    MainFooterComponent,
    CreateProjectsComponent,
    CsdProjectsComponent,
    ProjectTileComponent,
    AppreciationComponent,
    EmployeeTileComponent,
    NotificationsComponent,
    NotificationTileComponent
  ],
  bootstrap: [AppComponent]
})

export class AppModule {}
