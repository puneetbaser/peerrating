import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { AngularFire } from 'angularfire2';

@Injectable()
export class RatingsService {

    private _ratingsUrl = "app/ratings"; // Mock data url
    private headers = new Headers({'Content-Type': 'application/json'});

    constructor(private http: Http,
                public angularFire: AngularFire) { }

    getRatings(UserEmail){
      return this.angularFire.database.list('ratings')
              .map(res => res);
    }

    updateRating(ratingDetails){
       this.angularFire.database.list('ratings').push(ratingDetails);
    }
    getUserRatings(UserEmail){
      return this.angularFire.database.object('employees')
              .map(res => {
                   var empEmail = UserEmail.replace(/[.]/g, "-");
                   var Rating;
                   var Qualities = ["CREATIVITY","QUALITY", "EXPERIMENTATION", "TIMELINESS", "TEAM WORK", "TRANSPARENCY"];
                   var IndividualRatings=[];
                   console.log(res[`${empEmail}`]);
                   if(res[`${empEmail}`]['overall_rating']>-1){
                      Rating = res[`${empEmail}`]['overall_rating'];
                   }
                   else{
                     Rating = 0;
                   }
                   for(var i=0; i<6; i++){
                     IndividualRatings.push(0);
                   }
                   /*for(var i=0; i<empLen; i++){
                     if(res[`${employeesKeys[i]}`]['email_id'] == UserEmail){
                       Rating = res[i].overall_rating;
                       var strengths = res[i].strengths;
                       for(var j=0; j< strengths.length; j++){
                         IndividualRatings.push(strengths[j].percent);
                         Qualities.push(strengths[j].quality);
                       }
                     }
                   }*/
                   return [Rating, IndividualRatings, Qualities];
               });
    }

    /***************** CODE WRITTEN WITH PROMISES ************************************
    getRatings() : Promise<any>{
        return this.http.get(this._ratingsUrl)
               .toPromise()
               .then(response => response.json().data as any)
               .catch(this.handleError);
    }

    updateRating(ratingDetails) : Promise<any>{
        console.log(ratingDetails);
        return this.http
            .post(this._ratingsUrl, JSON.stringify(ratingDetails), {headers: this.headers})
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    **********************************************************************************/

/******************** CODE FOR LOACAL DATABASE ***************************************
    getRatings() {
        return this.http.get(this._ratingsUrl)
               .map(res => res.json());
    }
**************************************************************************************/

}
