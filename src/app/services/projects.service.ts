import { Injectable, OnInit } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { AngularFire } from 'angularfire2';
import { ActivatedRoute, Router } from '@angular/router';

@Injectable()
export class ProjectsService implements OnInit{
  Months = ['January', 'February', 'March','April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  check = true;
  CsdId :any;
  date = new Date();
  month: any;
  year: any = this.date.getFullYear();
  Id: any;

  private _projectsUrl = "app/projects"; // Mock data url
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http,
              public angularFire: AngularFire,
              public router: Router) {

              if((this.date.getMonth()) == 0){
                this.month = this.Months[11];
                this.year = this.year - 1;
              }
              else{
                this.month = this.Months[(this.date.getMonth())];
              }
              this.Id = `${this.month}-${this.year}`;

              }
  getCsdProjects(UserEmail){
    var userEmail = UserEmail.replace(/[.]/g, "-");
    this.CsdId = userEmail;
    return this.angularFire.database.object('projects list')
         .map(res => {
           if(res != undefined){
             var CsdProjects = [];
             if(res[`${userEmail}`] != undefined){
               var values = res[`${userEmail}`];
               var valuesKeys = Object.keys(values);
               var valuesKeysLen = valuesKeys.length;
               for(var i=0; i<valuesKeysLen; i++){
                    CsdProjects.push(values[valuesKeys[i]]);
               }
               return [CsdProjects, valuesKeys];
             }
           }
         })
  }

  getProjects(UserEmail){
    var userEmail = UserEmail.replace(/[.]/g, "-");
    this.CsdId = userEmail;
    var deleteProjectKey;

    return this.angularFire.database.object('project-subscriptions')
       .map(res =>{
          var UserProjects = [];
          if(res[`${userEmail}`] != undefined) {
            if((res[`${userEmail}`][`${this.Id}`]) != undefined){
              var values = res[`${userEmail}`][`${this.Id}`];
              var valuesKeys = Object.keys(values);
              var valuesKeysLen = valuesKeys.length;
              for(var i=0; i<valuesKeysLen; i++){
                  UserProjects.push(values[valuesKeys[i]]);
              }
            }
        }
        return [UserProjects, valuesKeys];
    });
  }
  
  getProjectsList(){
    return this.angularFire.database.object('projects list')
         .map(res =>{
             var ProjectNames = [];
             var ProjectKeys = [];
             var CsdIds = [];
             var outerObjectKeys = Object.keys(res);
             var outerObjectKeysLen = (outerObjectKeys.length) - 2;
             for(var i=0; i< outerObjectKeysLen; i++){
               CsdIds.push(outerObjectKeys[i]);
               var innerObjectKeys = Object.keys(res[outerObjectKeys[i]]);
               var innerObjectKeysLen = (innerObjectKeys.length) ;
               for(var j=0; j< innerObjectKeysLen; j++){
                  ProjectNames.push(res[outerObjectKeys[i]][innerObjectKeys[j]]['project_name']);
                  ProjectKeys.push(innerObjectKeys[j]);
               }
             }
             return [ProjectNames, ProjectKeys, CsdIds];
         });
  }

  getClients(){
    return this.angularFire.database.object('clients')
            .map(res => {
                  var ClientNames = [];
                  var ClientId = [];
                  var objKeys = Object.keys(res);
                  var objKeysLen = (objKeys.length)-2;
                  for(var i=0; i< objKeysLen; i++){
                    ClientNames.push(res[objKeys[i]]['client_name']);
                    ClientId.push(objKeys[i]);
                  }
            return [ClientId, ClientNames];
            });
  }
  getLocations(){
    return this.angularFire.database.object('locations')
          .map(res => {
               var Locations = [];
               var objKeys = Object.keys(res);
               var objKeysLen = (objKeys.length)-2;
               for(var i=0; i< objKeysLen; i++){
                 Locations.push(res[objKeys[i]]);
               }
               return Locations;
          });
  }

  createProject(project){
   this.angularFire.auth.subscribe(user => {
      if(user){
        this.CsdId = (user.auth.email).replace(/[.]/g, "-");
      }
    });
    this.angularFire.database.list('projects list/'+`${this.CsdId}`).push(project);
  }

  deleteProject(Key, deleteProject, UserEmail){
    var userEmail = UserEmail.replace(/[.]/g , "-");
    this.angularFire.database.list(`project-subscriptions/`+`${userEmail}/`+`${this.Id}`).remove(`${Key}`);
  }

  deleteCsdProject(Key, UserEmail){
    var userEmail = UserEmail.replace(/[.]/g , "-");
    this.angularFire.database.list(`projects list/`+`${userEmail}`).remove(`${Key}`);
  }

  updateCsdProject(ProjectData, UserEmail, Key){
    var userEmail = UserEmail.replace(/[.]/g , "-");
    this.angularFire.database.list(`projects list/`+`${userEmail}`).update(`${Key}`, ProjectData);
    
  }

  AddProject(projectDetails, UserEmail){
    var userEmail = UserEmail.replace(/[.]/g , "-");
    this.angularFire.database.list('project-subscriptions/'+`${userEmail}/`+`${this.Id}`).push(projectDetails);
  }

  UpdateProject(UserEmail, Key, ProjectData){
     var userEmail = UserEmail.replace(/[.]/g , "-");
     this.angularFire.database.list('project-subscriptions/'+`${userEmail}/`+`${this.Id}`).update(`${Key}`, ProjectData);
  }

  ngOnInit(){ }
}
