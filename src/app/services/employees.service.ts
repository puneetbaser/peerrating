import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { AngularFire } from 'angularfire2';


@Injectable()
export class EmployeesService {

    Months = ['January', 'February', 'March','April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    currentProjects = [];
    Id: any = "";
    month: any;
    newStatus : any;

    private _employeesUrl = "app/employees"; // Mock data url

    constructor(private http: Http,
                public angularFire: AngularFire) { 
             
                var date = new Date().getMonth();
                this.month = this.Months[date];   
                var year = new Date().getFullYear();
                if(date == 0){
                  year = year - 1;
                  this.month = "December";
                  this.Id = `December-${year}`;
                }    
                else{
                  this.Id = `${this.month}-${year}`;
                }        
  


                }

    getAllEmployees(){
      return this.angularFire.database.list('employees')
             .map(res => res);
    }

    getEmployeesAppreciation(UserEmail){
      var userEmail = UserEmail.replace(/[.]/g, "-");
      return this.angularFire.database.object('appreciations')
          .map(res => {
                var AllEmpApp = [];
                var Keys = [];
                var objKeys = Object.keys(res);
                var objKeysLen = (objKeys.length) - 2;
                for(var i=0; i< objKeysLen; i++){
                  if(objKeys != userEmail){
                       Keys.push(objKeys[i]);
                       AllEmpApp.push(res[objKeys[i]]);
                  }
                }
                return [AllEmpApp, objKeys];
      });
    }

    getEmployeesDetails(UserEmail){
       var userEmail = UserEmail.replace(/[.]/g, "-");
       return this.angularFire.database.object('employees')
             .map(res => {
                  var AllEmpNames = [];
                  var AllEmpRoles = [];
                  var Keys = Object.keys(res);
                  var EmpIds = [];
                  var KeysLen = (Keys.length) - 2;
                  for(var i=0; i<KeysLen; i++){
                    EmpIds.push(Keys[i]);
                    if(Keys[i] != userEmail){
                      AllEmpNames.push(res[`${Keys[i]}`]['first_name']);
                      AllEmpRoles.push(res[`${Keys[i]}`]['role']);
                    }
                  }
                  return [AllEmpNames, AllEmpRoles, EmpIds];
           });
    }

    Upvote(Key, upvotes, downvotes){
      var updatedData = {
                           'likes_up': upvotes,
                           'likes_down': downvotes
                         };
      this.angularFire.database.list('appreciations').update(`${Key}`, updatedData);
    }

    DownVote(Key, upvotes,downvotes){
      var updatedData = {
                      'likes_up': upvotes,
                      'likes_down': downvotes
                    };
      this.angularFire.database.list('appreciations').update(`${Key}`, updatedData);
    }
    Comment(UserEmail, comment){
         var Comment = {
                          'comment' : comment
                        };
         this.angularFire.database.list(`appreciations/${UserEmail}/Comment`).push(comment);
    }

    ApproveProject(UserEmail, Key){
         var userEmail = UserEmail.replace(/[.]/g, "-");
         var status = 'Approved';
         var newStatus = {
           'status' : "Approved"
         }
         this.angularFire.database.object(`project-subscriptions/${userEmail}/${this.Id}/${Key}`).$ref.update(newStatus);
    }

    RejectProject(UserEmail, Key){
         var userEmail = UserEmail.replace(/[.]/g, "-");
         var status = 'Rejected';
         var newStatus = {
           'status' : "Rejected"
         }
         this.angularFire.database.object(`project-subscriptions/${userEmail}/${this.Id}/${Key}`).$ref.update(newStatus);
    }

    getApprovalList(){
      return this.angularFire.database.object('project-subscriptions')
             .map(emp => {
                   var startDates = [];
                   var endDates = [];
                   var projectNames = [];
                   var Ids = [];
                   var Keys = [];
                   var objKeys = Object.keys(emp);
                   var objKeysLen = (objKeys.length) - 2;
                   for(var i=0; i< objKeysLen; i++){
                      Ids.push(objKeys[i]);
                      var employeeMonthlyData = emp[objKeys[i]][`${this.Id}`];
                      var innerObjKeys = Object.keys(employeeMonthlyData);
                      var innerObjKeysLen = (innerObjKeys.length);
                      for(var j=0; j<innerObjKeysLen; j++){
                        if((employeeMonthlyData[innerObjKeys[j]]['status']) == "Pending"){
                          startDates.push(employeeMonthlyData[innerObjKeys[j]]['project_start']);
                          endDates.push(employeeMonthlyData[innerObjKeys[j]]['project_end']);
                          projectNames.push(employeeMonthlyData[innerObjKeys[j]]['project_name']);
                          Keys.push(innerObjKeys[j]);
                        }
                      }
                   }
                  console.log(Keys);
                  return [ Ids, Keys, startDates, endDates, projectNames ];                   

             });
    }

    getNames( Ids, Keys, startDates, endDates, projectNames ){
        return this.angularFire.database.object('employees')
            .map(res =>{  
                          var Names = [];
                          var finalData = [];
                          var objKeysLen = Ids.length;
                          for(var i=0; i<objKeysLen; i++){
                            Names.push(res[Ids[i]]['first_name']);
                          }
                          for(var i=0; i< objKeysLen; i++){
                             finalData.push(
                               {
                                 'Name' : Names[i],
                                 'StartDate': startDates[i],
                                 'EndDate': endDates[i],
                                 'ProjectName': projectNames[i],
                                 'Month' : this.month,
                                 'Key' : Keys[i]
                               }
                             );
                          }
                          return finalData;
            });
    }

    getEmployees(UserEmail){
      return this.angularFire.database.list('')
              .map(res => {
                var userEmail = UserEmail.replace(/[.]/g, "-");
                var CSDLogin = [];
                var dataKeys = [];
                var date = new Date();
                var month;
                var year = date.getFullYear();
                if((date.getMonth()) == 0){
                  month = this.Months[11];
                  year = year - 1;
                }
                else{
                  month = this.Months[(date.getMonth())];
                }
                var Id = `${month}-${year}`;
                for(var key in res){
                  dataKeys.push(res[`${key}`][`$key`]);
                }
                var projects =[];
                var employeesDataId = dataKeys.indexOf('employees');
                var projectsDataId = dataKeys.indexOf('projects');
                var projectsListDataId = dataKeys.indexOf('projects list');
                var projectsSubscriptionsId = dataKeys.indexOf('project-subscriptions');
                var ratingsDataId = dataKeys.indexOf('ratings');
                var projectsLen = 0;
                if(projectsSubscriptionsId > -1){
                  if(res[projectsSubscriptionsId][`${userEmail}`] != undefined){
                    if(res[projectsSubscriptionsId][`${userEmail}`][`${Id}`] != undefined){
                      var UserProjects = res[projectsSubscriptionsId][`${userEmail}`][`${Id}`];
                      var ProjKeys = Object.keys(UserProjects);
                      for(var i=0; i<ProjKeys.length; i++){
                        projects.push(UserProjects[ProjKeys[i]]['project_id']);
                      }
                    }
                  }
                }
                if(employeesDataId > -1){
                  if(res[employeesDataId][userEmail]['role'] == 'CSD'){
                    CSDLogin.push(true);
                  }
                }

                var employees = [];
                var empProjects = [];
                var employeesLen = 0;
                if(employeesDataId > -1){
                    var employeesKeys = (Object.keys(res[employeesDataId]));
                    employeesLen = (Object.keys(res[employeesDataId]).length)-2;
                    for(var i=0; i<projects.length; i++){
                        for(var j=0; j<employeesLen; j++){
                           var empEmail = (res[employeesDataId][`${employeesKeys[j]}`]['email_id']).replace(/[.]/g, "-");
                           if((res[employeesDataId][`${employeesKeys[j]}`]['email_id'] == UserEmail) && ((res[employeesDataId][`${employeesKeys[j]}`]['role'] == "CSD"))){
                             CSDLogin.push(true);
                           }
                           if(res[employeesDataId][`${employeesKeys[j]}`]['email_id'] != UserEmail){
                               this.currentProjects = [];
                               if(res[projectsSubscriptionsId][`${empEmail}`] != undefined){
                                   var x = res[projectsSubscriptionsId][`${empEmail}`][`${Id}`];
                                   if(x== undefined){
                                     continue;
                                   }
                                   var xKeys = Object.keys(x);
                                   for(var k=0; k<((xKeys.length)); k++){
                                      this.currentProjects.push(x[`${xKeys[k]}`]['project_id']);
                                   }
                                   if((this.currentProjects.indexOf(projects[i]))>-1){
                                      employees.push(res[employeesDataId][`${employeesKeys[j]}`]);
                                      empProjects.push(projects[i]);
                                   }
                                }
                              }
                        }
                    }
                }

                var ratingsLen = 0;
                var ratings = [];
                if(ratingsDataId > -1){
                    ratingsLen = (Object.keys(res[ratingsDataId]).length)-2;
                    var UserEmailLower = UserEmail.toLowerCase();
                    var ratingKeys = Object.keys(res[ratingsDataId]).slice(0, ratingsLen);
                    var date = new Date();
                    var month;
                    var year = date.getFullYear();
                    if((date.getMonth()) == 0){
                      month = this.Months[11];
                      year = year - 1;
                    }
                    else{
                      month = this.Months[(date.getMonth())];
                    }
                    for(var i=0; i<employees.length; i++){
                      var beforeLength = ratings.length;
                      for(var j=0; j< ratingsLen; j++){
                        var EmpEmailLower = (employees[i].email_id).toLowerCase();
                        var RatingEmailLower = (res[ratingsDataId][`${ratingKeys[j]}`].email_id).toLowerCase();
                        var RatingFromEmailLower = (res[ratingsDataId][`${ratingKeys[j]}`].from_email_id).toLowerCase();
                        if((RatingFromEmailLower==UserEmailLower)
                           && (RatingEmailLower == EmpEmailLower)
                           && ((res[ratingsDataId][`${ratingKeys[j]}`].project_id == empProjects[i]))
                           && ((res[ratingsDataId][`${ratingKeys[j]}`].month == month))
                           ){
                             ratings.push(res[ratingsDataId][`${ratingKeys[j]}`].rating);
                             break;
                        }
                      }
                      var afterLength = ratings.length;
                      if(afterLength == beforeLength){
                        ratings.push(0);
                      }
                    }
                }
                else{
                  for(var i=0; i< employees.length; i++){
                    ratings.push(0);
                  }
                }
                return [employees, empProjects, ratings, CSDLogin];
               });
    }

/*********   CODE WRITTEN WITH PROMISES  *************

    getEmployees() : Promise<any>{
        return this.http.get(this._employeesUrl)
               .toPromise()
               .then(response => response.json().data as any)
               .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
********************************************************/


/*************** CODE FOR LOCAL DATABASE ****************
    /*getEmployees() {
       return this.http.get(this._employeesUrl)
              .map(res => res.json());
    }
*********************************************************/

}
