import { InMemoryDbService } from '@angular/../angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
    createDb(){
        let employees = [
            {
                "email_id": "puneet@moonraft.com",
                "firstName": "PUNEET",
                "last_name": "Baser",
                "mobile": "9590808875",
                "country": "India",
                "stream": "Engineering",
                "role": "Engagement lead",
                "dob": "06/11/1988",
                "profile_pic": "./../../images/puneet.jpg",
                "overall_rating" : 4.2,
                "ratings" : [
                  {
                    "quality" : "CREATIVITY",
                    "percent" : 40
                  },
                  {
                    "quality" : "EXPERIMENTATION",
                    "percent" : 40
                  },
                  {
                    "quality" : "PROBLEMSOLVING",
                    "percent" : 40
                  },
                  {
                    "quality" : "QUALITY",
                    "percent" : 40
                  },
                  {
                    "quality" : "TIMELINESS",
                    "percent" : 40
                  },
                  {
                    "quality" : "TRANSPARENCY",
                    "percent" : 40
                  }
                 ],
                "projects":[
                {
                    "project_id": "PEER RATING",
                    "project_members":["aman@moonraft.com", "vijay@moonraft.com"]
                },
                {
                    "project_id": "l&tfinance",
                    "project_members":["vasu@moonraft.com"]
                }
                ],
                "timestamp": ""
            },
            {
                "email_id": "aman@moonraft.com",
                "firstName": "AMAN",
                "last_name": "Kumar",
                "mobile": "9590808875",
                "country": "India",
                "stream": "Engineering",
                "role": "Project Lead",
                "dob": "06/11/1988",
                "profile_pic": "./../../images/puneet.jpg",
                "overall_rating" : 4.2,
                "ratings" : [
                  {
                    "quality" : "CREATIVITY",
                    "percent" : 40
                  },
                  {
                    "quality" : "EXPERIMENTATION",
                    "percent" : 40
                  },
                  {
                    "quality" : "PROBLEMSOLVING",
                    "percent" : 40
                  },
                  {
                    "quality" : "QUALITY",
                    "percent" : 40
                  },
                  {
                    "quality" : "TIMELINESS",
                    "percent" : 40
                  },
                  {
                    "quality" : "TRANSPARENCY",
                    "percent" : 40
                  }
                 ],
                "projects":[
                {
                    "project_id": "ICICI BANKING",
                    "project_members":["puneet@moonraft.com", "vijay@moonraft.com"]
                }
                ],
                "timestamp": ""
            },
            {
                "email_id": "vijay@moonraft.com",
                "firstName": "VIJAY",
                "last_name": "Sajjan",
                "mobile": "9590808875",
                "country": "India",
                "stream": "Engineering",
                "role": "Developer",
                "dob": "06/11/1988",
                "profile_pic": "url",
                "overall_rating" : 4.2,
                "ratings" : [
                  {
                    "quality" : "CREATIVITY",
                    "percent" : 40
                  },
                  {
                    "quality" : "EXPERIMENTATION",
                    "percent" : 40
                  },
                  {
                    "quality" : "PROBLEMSOLVING",
                    "percent" : 40
                  },
                  {
                    "quality" : "QUALITY",
                    "percent" : 40
                  },
                  {
                    "quality" : "TIMELINESS",
                    "percent" : 40
                  },
                  {
                    "quality" : "TRANSPARENCY",
                    "percent" : 40
                  }
                 ],
                "projects":[
                {
                    "project_id": "L&T FINANCE",
                    "project_members":["aman@moonraft.com", "puneet@moonraft.com"]
                },
                {
                    "project_id": "ICICI",
                    "project_members":["vasu@moonraft.com"]
                }
                ],
                "timestamp": ""
            },
            {
                "email_id": "mohammed@moonraft.com",
                "firstName": "MOHAMMED",
                "last_name": "FURQAN RAHAMATH M",
                "mobile": "8762728748",
                "country": "India",
                "stream": "Engineering",
                "role": "Developer",
                "dob": "06/06/1994",
                "profile_pic": "url",
                "overall_rating" : 4.2,
                "ratings" : [
                  {
                    "quality" : "CREATIVITY",
                    "percent" : 70
                  },
                  {
                    "quality" : "EXPERIMENTATION",
                    "percent" : 90
                  },
                  {
                    "quality" : "PROBLEMSOLVING",
                    "percent" : 80
                  },
                  {
                    "quality" : "QUALITY",
                    "percent" : 50
                  },
                  {
                    "quality" : "TIMELINESS",
                    "percent" : 25
                  },
                  {
                    "quality" : "TRANSPARENCY",
                    "percent" : 35
                  }
                 ],
                "projects":[
                {
                    "project_id": "PEER RATING",
                    "project_members":["aparna@moonraft.com", "puneet@moonraft.com"]
                }
                ],
                "timestamp": ""
            }
        ];
        let ratings = [
            {
                "email_id": "puneet@moonraft.com",
                "year": "2016",
                "month": "october",
                "project_id": "icici_tab_banking",
                "from_email_id": "vijay@moonraft.com",
                "rating": 3,
                "strength": [
                "team work",
                "temperament",
                "leadership"
                ],
                "weakness": [
                "quality"
                ]
            },
            {
                "email_id": "puneet@moonraft.com",
                "year": "2016",
                "month": "october",
                "project_id": "icici_tab_banking",
                "from_email_id": "mohammed@moonraft.com",
                "rating": 4,
                "strength": [
                "team work",
                "leadership"
                ],
                "weakness": [
                "quality"
                ]
            }

        ];
        let projects = [
            /*{
              "project_id" : "TATA AIG",
              "email_id" : "mohammed@moonraft.com",
              "year" : "2016",
              "month": "November",
              "project_start" : 1,
              "project_end"   : 14
            }*/
        ];
        return {employees, ratings, projects};
    }
}
