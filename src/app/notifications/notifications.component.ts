import { Component, OnInit } from '@angular/core';
import { EmployeesService } from './../services/employees.service';
import { Router } from '@angular/router';
import { AngularFire, AuthProviders } from 'angularfire2';


@Component({
  selector: "notifications",
  templateUrl: "./notifications.component.html",
  styleUrls: ["./notifications.component.scss"],
  providers: [ EmployeesService ]
})

export class NotificationsComponent implements OnInit{
     
     NotificationData = [];

     constructor(public _employeesService: EmployeesService,
              public angularFire: AngularFire,
              public router: Router) {

              }

   getApprovalList(){
       this._employeesService.getApprovalList()
          .subscribe( data =>{
                console.log(data[0]);
                console.log(data[2]);
                console.log(data[3]);
                if((data[2].length) != 0){
                    this.getNames(data[0], data[1], data[2], data[3], data[4]);
                }  
        });
   }
   getNames(Ids, Keys, startDates, endDates, projectNames){
       this._employeesService.getNames(Ids, Keys, startDates, endDates, projectNames)
         .subscribe( data => {
                console.log(data);
                this.NotificationData = data;
         });
   }

 ngOnInit(){
   this.getApprovalList();
 }

}