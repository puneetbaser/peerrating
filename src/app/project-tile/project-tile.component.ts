import { Component, OnInit, Input } from '@angular/core';
import { AngularFire, AuthProviders } from 'angularfire2';
import { ProjectsService } from './../services/projects.service';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'project-tile',
  templateUrl: './project-tile.component.html',
  styleUrls: ['./project-tile.component.scss'],
  providers: [ ProjectsService ]
})
export class ProjectTileComponent implements OnInit {
  @Input() Project: any;
  @Input() Key: any;
  @Input() client: any = "Moonraft";
  @Input() location: any = "Bangalore";
  @Input() project_type: any = "Internal";
  Duration: any = "70%";
  RangeBeginning: any = "5%";
  UserEmail: any;

  constructor(public angularFire: AngularFire,
              public _projectsService: ProjectsService,
              public router: Router) {
     
          this.angularFire.auth.subscribe( user =>{
            if(user){
              this.UserEmail = user.auth.email;
            }
          });
  }

  deleteCsdProject(){
    this.angularFire.auth.subscribe(user => {
      if(user) {
        this.UserEmail = user.auth.email;
        this._projectsService.deleteCsdProject(this.Key, this.UserEmail);
      }
    });
  }

  editProject(){
    alert('Functionality not added yet');
    let navigationExtras : NavigationExtras = {
          queryParams: {
              "CsdProjectKey": this.Key
          }
    }
    this.router.navigate(["create-projects"], navigationExtras);
    
  }

  ngOnInit() {
  }

}
