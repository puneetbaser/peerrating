import { Component, OnInit, Input } from '@angular/core';
import { AngularFire, AuthProviders } from 'angularfire2';
import { ProjectsService } from './../services/projects.service';
import { EmployeesService } from './../services/employees.service';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'notification-tile',
  templateUrl: './notification-tile.component.html',
  styleUrls: ['./notification-tile.component.scss'],
  providers: [ ProjectsService, EmployeesService ]
})

export class NotificationTileComponent implements OnInit{
    ngOnInit(){}
    UserEmail: any;
    @Input() Name: any = "MOHAMMED";
    @Input() Project: any = "PEER RATING";
    @Input() Start: any = "9";
    @Input() End: any = "26";
    @Input() Month: any = "September";
    @Input() Key: any ;

    Approve(){
        console.log(this.Key);
        var project = {
                         
                       };
        this._employeesService.ApproveProject(this.UserEmail, this.Key );
    }

    Reject(){
        console.log(this.Key);
        this._employeesService.RejectProject(this.UserEmail, this.Key);
    }

    constructor(public angularFire: AngularFire,
            public _projectsService: ProjectsService,
            public _employeesService: EmployeesService,
            public router: Router) {
    
        this.angularFire.auth.subscribe( user =>{
        if(user){
            this.UserEmail = user.auth.email;
        }
        });
  }
}