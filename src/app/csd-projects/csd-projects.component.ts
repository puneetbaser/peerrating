import { Component, OnInit, ElementRef } from '@angular/core';
import { ProjectsService } from './../services/projects.service';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { ViewEncapsulation } from '@angular/core';
import { AngularFire } from 'angularfire2';
import { MainFooterComponent } from './../main-footer/main-footer.component';


@Component({
  selector: "csd-projects",
  templateUrl: "./csd-projects.component.html",
  styleUrls: ["./csd-projects.component.scss"],
  providers: [ ProjectsService ]
})
export class CsdProjectsComponent implements OnInit {
  Project: any = "PEER RATING";
  Projects: any;
  UserEmail: any;
  ProjectKeys = [];
  csdLogin: any = false;
  userEmail: any;
  constructor(private _projectsService: ProjectsService,
              private elementRef: ElementRef,
              private route: ActivatedRoute,
              private router: Router,
              public angularFire: AngularFire) {

          this.angularFire.auth.subscribe( user =>{
            if(user){
              this.UserEmail = user.auth.email;
            }
          });
 }

 addProject(){
    this.router.navigate(['create-projects']);
 }
  ngOnInit() {
    this.getCsdProjects();
  }

  getCsdProjects(){
        this._projectsService.getCsdProjects(this.UserEmail)
        .subscribe(data => { 
              if(data != undefined){
                   this.Projects = data[0];
                   this.ProjectKeys = data[1];
              }
         });
  }

}
