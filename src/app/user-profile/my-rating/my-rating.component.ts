import { Component, OnInit } from '@angular/core';
import { RatingMeterComponent } from './../rating-meter/rating-meter.component';
import { EmployeesService } from '../../services/employees.service';
import { RatingsService } from '../../services/ratings.service';
import { AngularFire, AuthProviders } from 'angularfire2';
import { ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-my-rating',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './my-rating.component.html',
  styleUrls: ['./my-rating.component.scss'],
  providers: [ EmployeesService, RatingsService ]
})
export class MyRatingComponent implements OnInit {

  color1: string = "$primary-active-icon";
  color2: string = "blue";
  radius1: string = "10";
  radius2: string = "6";
  Type1: string = "Rating";
  Type2: string = "Quality";
  Rating: any = 0;
  employees = [];
  RatingDetails = [];
  IndividualRatings = [0, 0, 0, 0, 0, 0];
  Qualities = [];
  UserEmail: any;
  month: any;
  temp : any;
  Months = ['January', 'February', 'March','April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  displayProjects : any = false;
  displayRatings : any = true;
  meterBig: any = "meter-big";
  Id = 99;
  constructor(private _ratingsService: RatingsService,
              public angularFire: AngularFire) {
          this.month = new Date();
          this.month= this.month.getMonth();
          if(this.month == 0){
            this.month = this.Months[11];
          }
          else{
            this.month = this.Months[this.month];
          }
          angularFire.auth.subscribe(user =>
              {
                if(user){
                this.UserEmail = user.auth.email;
               }
             }
          );

 }
  ngOnInit() {
     this.getRatingDetails();
  }

  showRatings(){
    this.displayRatings = true;
    this.displayProjects = false;
  }

  showProjects(){
    this.displayRatings = false;
    this.displayProjects = true;
  }

  getRatingDetails(){
    this._ratingsService.getUserRatings(this.UserEmail)
     .subscribe(data => {
                this.Rating = data[0];
                this.IndividualRatings = data[1];
                this.Qualities = data[2];

     });
  }


}
