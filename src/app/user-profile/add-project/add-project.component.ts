import { Component, OnInit } from '@angular/core';
import { SelectModule } from './../../../../node_modules/angular2-select';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ProjectsService } from './../../services/projects.service';
import { AngularFire } from 'angularfire2';


@Component({
  selector: 'add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.scss'],
  providers: [ ProjectsService ]
})
export class AddProjectComponent implements OnInit {
  monthNumber: any;
  month: any;
  year : any;
  temp : any;
  userEmail: any;
  Months = ['January', 'February', 'March','April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  options = [];
  project: any;
  startDate: any;
  endDate: any;
  SelectedDates: any = " -- Select Date -- ";
  dateDark = false;
  UpdatedProjects: any;
  ProjectsList = [];
  Projects = [];
  ProjectKeys = [];
  CsdIds = [];
  CsdId: any;
  ProjectKey: any;
  ProjectName: any;
  listDisplay: any = false;
  displayValue : any;
  UserEmail: any;
  ProjectEditKey: any = "";
  constructor(private router: Router,
              private route: ActivatedRoute,
              public angularFire: AngularFire,
              private _projectsService: ProjectsService
              ) {

              this.month = new Date();
              this.year  = this.month.getFullYear();
              this.monthNumber= this.month.getMonth();
              if(this.monthNumber == 0){
                this.monthNumber = 12
                this.month = this.Months[this.monthNumber - 1];
                this.year = this.year - 1;
              }
              else{
                this.month = this.Months[this.monthNumber - 1];
                this.monthNumber = this.monthNumber + 1;
              }
              this.angularFire.auth.subscribe(user => {
                if(user){
                  this.UserEmail = user.auth.email;
                }
              });
              this.route.queryParams.subscribe( params => {
                    this.ProjectEditKey = params["ProjectEditKey"];
                    this.project = params["Project"];
                    this.startDate = params["StartDate"];
                    this.endDate = params["EndDate"];
                    this.ProjectKey = params["ProjectKey"];
                    this.CsdId = params["CsdId"];
                    if(this.startDate != undefined){
                        this.dateDark = true;
                        this.SelectedDates = `${this.startDate}-${this.monthNumber}-${this.year} to ${this.endDate}-${this.monthNumber}-${this.year}`;
                    }
              });
 }

 SelectDates(){
   let navigationExtras : NavigationExtras = {
         queryParams: {
              "Project": this.project,
              "ProjectEditKey": this.ProjectEditKey,
              "CsdId": this.CsdId,
              "ProjectKey": this.ProjectKey
         }
   }
   this.router.navigate( ["calendar"], navigationExtras);
 }

 generateProjectsList(Projects, Keys, CsdIds){

   for(var i=0; i<Projects.length; i++){
      this.CsdIds.push(CsdIds[i]);
      this.ProjectKeys.push(Keys[i]);
      this.Projects.push(
        {
          value: i,
          label: Projects[i]
        }
      );
   }
 }
 showList(val,x){
   this.displayValue = x.label;
   this.project = x.label;
   this.ProjectKey = this.ProjectKeys[x.value];
   this.CsdId = this.CsdIds[x.value];
   this.listDisplay = !this.listDisplay;
 }
 getProjectsList(){
    this._projectsService.getProjectsList()
        .subscribe( data => {this.generateProjectsList(data[0], data[1], data[2])});
 }
 _projectDetails;
 AddProject(): void{
   this._projectDetails = {
         "project_id" : this.ProjectKey,
         "project_start" : this.startDate,
         "project_end" : this.endDate,
         "status" : "Pending",
         "csd_id" : this.CsdId,
         "project_name" : this.project
     };
    if((this.ProjectEditKey =="") || (this.ProjectEditKey == undefined) || (this.ProjectEditKey == 'undefined')){
        this._projectsService.AddProject(this._projectDetails, this.UserEmail);
    }
    else{
        this._projectsService.UpdateProject(this.UserEmail, this.ProjectEditKey, this._projectDetails);
    }
    this.router.navigate(["my-project"]);

 }

/***************** INITIAL CODE (NOT USED) ************************
 goBack(): void{
   console.log("Successfully saved");
   this._projectsService.getProjects()
               .subscribe(data=> this.UpdatedProjects = data.data,
                          error => alert(error),
                          () => console.log(this.UpdatedProjects));
 }
******************************************************************/

  ngOnInit() {
    if(this.project == undefined){
      this.displayValue = 'Project name';
    }
    else{
      this.displayValue = this.project;
    }
    this.getProjectsList();
  }
}
