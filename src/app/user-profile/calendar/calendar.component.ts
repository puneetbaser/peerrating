import { Component, OnInit } from '@angular/core';
import { ElementRef } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';


declare var $:any;

@Component({
  selector: 'calendar',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
})
export class CalendarComponent implements OnInit {
  month: any;
  temp : any;
  Months = ['January', 'February', 'March','April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  options: any;
  Project: any;
  startDate: any;
  endDate: any;
  ProjectEditKey: any = "";
  CsdId: any;
  ProjectKey: any;

  constructor(private route: ActivatedRoute,
              private router: Router
              ) {

                this.month = new Date();
                this.month= this.month.getMonth();
                if(this.month == 0){
                  this.month = this.Months[11];
                }
                else{
                  this.month = this.Months[this.month];
                }
                this.route.queryParams.subscribe( params => {
                      this.Project = params["Project"];
                      this.ProjectEditKey = params["ProjectEditKey"];
                      this.ProjectKey = params["ProjectKey"];
                      this.CsdId = params["CsdId"];
                });
  }

  SendDates(){
    var start = document.getElementsByClassName('pignose-calendar-unit-range-first')[0];
    var end = document.getElementsByClassName('pignose-calendar-unit-range-last')[0];
    this.startDate = start.getElementsByTagName('A')[0].innerHTML;
    this.endDate = end.getElementsByTagName('A')[0].innerHTML;
    let navigationExtras : NavigationExtras = {
          queryParams: {
               "Project"  : this.Project,
               "StartDate": this.startDate,
               "EndDate"  : this.endDate,
               "ProjectEditKey": this.ProjectEditKey,
               "ProjectKey": this.ProjectKey,
               "CsdId" : this.CsdId
          }
    }
    this.router.navigate( ["add-project"], navigationExtras);
  }

  ngOnInit() {

    $('.calender').pignoseCalendar();
    $('.calender').pignoseCalendar({

       // en, ko, fr, ch, de, jp, pt
       lang: 'en',


       // light, dark
       theme: 'light',

       // date format
       format: 'YYYY-MM-DD',

       // CSS class array
       classOnDays: [],

       // array of enabled dates
       enabledDates: [],

       // disabled dates
       disabledDates: [],

       // You can disable by using a weekday number array (This is a sequential number start from 0 [sunday]).
       disabledWeekdays: [],

       // This option is advanced way to using disable settings, You can give multiple disabled range by double array date string by formatted to 'YYYY-MM-DD'.
       disabledRanges: [],

       // If you set true this option, You can use multiple range picker by one click on your Calendar.
       pickWeeks: false,

       //  You can turn on/off initialized date by this option.
       initialize: false,

       // enable multiple date selection
       multiple: true,

       // use toggle in the calendar
       toggle: false,

       // reverse mode
       reverse: false,

       // shows buttons
       //buttons: true,

       // display calendar as modal
       modal: false,

       // add button controller for confirm by user
       buttons: false,

       // minimum disable date range by 'YYYY-MM-DD' formatted string
       minDate: null,

       // maximum disable date range by 'YYYY-MM-DD' formatted string
       maxDate: null,

       // called when you select a date on calendar (date click).
       //select: this.printDate(),

       // called when you apply a date on calendar. (OK button click).
       //apply: function ({}),

     });

  }

}
