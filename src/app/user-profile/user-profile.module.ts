import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { UserProfileRoutingModule } from './user-profile-routing.module';
import { UserProfileComponent } from './user-profile.component';
import { MyProjectComponent } from './my-project/my-project.component';
import { MyRatingComponent } from './my-rating/my-rating.component';
import { RatingMeterComponent } from './rating-meter/rating-meter.component';
import { AddProjectComponent } from './add-project/add-project.component';
import { CalendarComponent } from './calendar/calendar.component';
import { SelectModule } from './../../../node_modules/angular2-select';
import { ProjectTileComponent } from './project-tile/project-tile.component';
import { MainFooterComponent } from './main-footer/main-footer.component';


@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    UserProfileRoutingModule,
    SelectModule
  ],
  declarations: [
    UserProfileComponent,
    MyProjectComponent,
    MyRatingComponent,
    RatingMeterComponent,
    AddProjectComponent,
    CalendarComponent,
    ProjectTileComponent,
    MainFooterComponent
  ],
  providers: []
})
export class UserProfileModule {}
