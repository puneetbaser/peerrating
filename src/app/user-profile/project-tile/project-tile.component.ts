import { Component, OnInit, Input } from '@angular/core';
import { AngularFire, AuthProviders } from 'angularfire2';
import { ProjectsService } from './../../services/projects.service';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'project-tile',
  templateUrl: './project-tile.component.html',
  styleUrls: ['./project-tile.component.scss'],
  providers: [ ProjectsService ]
})
export class ProjectTileComponent implements OnInit {
  @Input() Project: any;
  @Input() StartDate: any = 1;
  @Input() EndDate : any = 30;
  @Input() UserEmail: any;
  @Input() Key: any;
  @Input() Status: any = "Pending";
  @Input() client: any = "Moonraft";
  @Input() location: any = "Bangalore";
  @Input() project_type: any = "Internal";
  Duration: any = "70%";
  RangeBeginning: any = "5%";

  constructor(public angularFire: AngularFire,
              public _projectsService: ProjectsService,
              private router: Router,
              private route: ActivatedRoute) {


  }

  deleteProject(){
    this.angularFire.auth.subscribe(user => {
      if(user) {
        this.UserEmail = user.auth.email;
        this._projectsService.deleteProject(this.Key, this.Project, this.UserEmail);
      }
    });
  }

  editProject(){
    alert('Functionality not added yet');
    let navigationExtras : NavigationExtras = {
      queryParams: {
          "ProjectEditKey": this.Key
      }
     }
     this.router.navigate( ["add-project"], navigationExtras);

  }

  ngOnInit() {
    this.Duration = `${(this.EndDate - this.StartDate)*(100/30)}%`;
    this.RangeBeginning = `${(this.StartDate-1)*3.2}%`;
  }

}
