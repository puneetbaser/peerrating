import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFire, AuthProviders } from 'angularfire2';
import { EmployeesService } from '../../services/employees.service';

@Component({
  selector: 'main-footer',
  templateUrl: './main-footer.component.html',
  styleUrls: ['./main-footer.component.scss']
})
export class MainFooterComponent implements OnInit {
  CSDLogin: any = false;
  active1: any;
  active2: any;
  active3: any = true;
  active4: any = false;
  listDisplay = false;
  display: any= false;
  createProjectsDisplay: any = false;
  rev : any = true
  UserEmail: any;
  Active(x){
    if(this.active4 == false){
      this.active4 = true;
    }
    else{
      this.active4 = false;
    }
  }
  constructor(public router: Router,
              private _employeeService: EmployeesService,
              public angularFire: AngularFire) {
          angularFire.auth.subscribe(user => {
            if(user) {
              this.UserEmail = user.auth.email;
            }
          });
   }
  getEmployees(){
    this._employeeService.getEmployees(this.UserEmail)
    .subscribe(data => {
                        this.CSDLogin = (data[3][0]);
                      });
  }
  logout() {
    this.angularFire.auth.logout();
  }
  appreciation(){
    console.log('coming');
    this.router.navigate(["appreciation"]);
  }
  Notifications(){
    this.router.navigate(['notifications']);
  }
  showList(){
    if(this.display == true){
      console.log('display is true');
      this.display = false;
    }
    else{
      console.log('display is false');
      this.display = true;
    }
    this.listDisplay =  this.display;
    if(this.CSDLogin == true){
      console.log('CSD');
      this.createProjectsDisplay = true;
      this.rev = false;
    }
  }

  CsdProjects(){
    this.router.navigate(['csd-projects']);
  }

  createProjects(){
    this.router.navigate(["create-projects"]);
  }



  ngOnInit() {
    this.getEmployees();
    this.display = false;
  }

}
