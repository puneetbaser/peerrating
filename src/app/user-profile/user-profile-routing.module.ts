import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyProjectComponent } from './my-project/my-project.component';
import { MyRatingComponent } from './my-rating/my-rating.component';
import { AddProjectComponent } from './add-project/add-project.component';
import { CalendarComponent } from './calendar/calendar.component';

/** Custom components **/

const routes : Routes = [
  {
    path: 'user-profile',
    redirectTo: '/my-project',
    pathMatch: 'full'
  },
  {
    path: 'my-project',
    component: MyProjectComponent
  },
  {
    path: 'my-rating',
    component: MyRatingComponent
  },
  {
    path: 'add-project',
    component: AddProjectComponent
  },
  {
    path: 'calendar',
    component: CalendarComponent
  }
];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class UserProfileRoutingModule{

}
