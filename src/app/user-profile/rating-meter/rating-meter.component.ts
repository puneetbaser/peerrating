import { Component, Input, OnInit } from '@angular/core';
import { RatingsService } from './../../services/ratings.service';

declare var $:any;

@Component({
  selector: 'app-rating-meter',
  templateUrl: './rating-meter.component.html',
  styleUrls: ['./rating-meter.component.scss'],
  providers: [  ]
})
export class RatingMeterComponent implements OnInit {
  @Input() InnerColor: string = '#fff';
  @Input() TextColor: string = '#000';
  @Input() UnfilledLineColor: string = '#ddd';
  @Input() radius: any = "100px";
  @Input() Percent: any = 50;
  @Input() Positive: any = 25;
  @Input() Negative: any = 45;
  @Input() MeterType: any = 'Rating';
  @Input() Quality: any ="";
  @Input() Id : any = "container";
  FontSize: any;
  Neutral : any = 100 - (this.Positive + this.Negative);


  Fontsize(){
    if(this.MeterType == 'Rating'){
      return "200px";
    }
    else{
      return "100px";
    }
  }

  constructor() {
  }
  ngOnInit() {
    this.FontSize = this.Fontsize();
    var Percents: any;
    var FontSize: any;
    if(this.Id ==99){
      this.Id = 0;
      this. Neutral = 100 - this.Positive;
      Percents = [
                   {'color': "#cccccc", 'perc': this.Neutral},
                   {'color': "#19d2a2", 'perc': this.Positive}
                 ];
      FontSize = "35";
    }
    else{
      this.Id = this.Id+1;
      FontSize = "0";
      Percents = [
                    {'color': "#f83e40", 'perc': this.Negative},
                    {'color': "#ffc107", 'perc': this.Neutral},
                    {'color': "#19d2a2", 'perc': this.Positive}
                  ];
    }
    $($('.meter').get(this.Id)).radialPieChart("init", {
       'fill': 7,
       'data': Percents,
       "inline": true,
       "font-size": FontSize,
       "font-family": "Helvetica, Arial, sans-serif",
       "text-color": null,
       "lines": 1,
       "line": 0,
       "symbol": "",
       "margin": 0,
       "size": this.Fontsize(),
       "range": [0, 100]
     });
  }
  }
