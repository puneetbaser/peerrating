import { Component, OnInit, ElementRef } from '@angular/core';
import { ProjectsService } from './../../services/projects.service';
import { ViewEncapsulation } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { AngularFire } from 'angularfire2';

@Component({
  selector: 'app-my-project',
  templateUrl: './my-project.component.html',
  styleUrls: ['./my-project.component.scss'],
  providers: [ ProjectsService ]
})
export class MyProjectComponent implements OnInit {
  month: any;
  temp : any;
  Months = ['January', 'February', 'March','April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  Project: any = "PEER RATING";
  StartDate: any = 3;
  EndDate : any =25;
  Projects: any;
  day: any;
  UserEmail: any;
  ProjectKeys = [];
  csdLogin: any = false;
  displayProjects : any = true;
  displayRatings : any = false;
  userEmail: any;
  constructor(private _projectsService: ProjectsService,
              private elementRef: ElementRef,
              private route: ActivatedRoute,
              private router: Router,
              public angularFire: AngularFire) {
          this.month = new Date();
          this.day = this.month.getDate();
          this.month= this.month.getMonth();
          if(this.month == 0){
            this.month = this.Months[11];
          }
          else{
            this.month = this.Months[this.month];
          }

          this.angularFire.auth.subscribe( user =>{
            if(user){
              this.UserEmail = user.auth.email;
            }
          });
 }

showRatings(){
  this.displayRatings = true;
  this.displayProjects = false;
}

showProjects(){
  this.displayRatings = false;
  this.displayProjects = true;
}

 addProject(){
   if((this.day>=1)&&(this.day<32)){
     this.router.navigate(["add-project"]);
   }
   else{
     if(this.day >31){
       alert("You can't subscribe to new projects");
     }
   }
 }

  ngOnInit() {
    this.route.queryParams.subscribe( params => {
        this.csdLogin = params["CSDLogin"];
    }
    );
    this.getProjects();
  }

  getProjects(){
        this._projectsService.getProjects(this.UserEmail)
        .subscribe(data => { if (data != null){
                              this.Projects = data[0];
                              this.ProjectKeys = data[1];
                            }
                          });
  }

}
