import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { EmployeesService } from './../services/employees.service';

@Component({
  selector: 'employee-tile',
  templateUrl: './employee-tile.component.html',
  styleUrls: ['./employee-tile.component.scss'],
  providers: [ EmployeesService ]
})
export class EmployeeTileComponent implements OnInit {
  @Input() upvotes: any;
  @Input() downvotes: any;
  @Input() employeeKey: any;
  @Input() employeeName: any;
  @Input() employeeRole: any;
  ShowComment : any = false;
  yes0 = false;
  yes1 = false;
  countUp = 0;
  countDown = 0;
  UpVotes : any = this.upvotes;
  DownVotes: any = this.downvotes;
  upvoteActive: any = false;
  downvoteActive: any = false;
  comment: any;


  ImageSrc = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ3tpHxLZOkETrMCgUWwY6_u3WEUbk5nsiHoU1d29-1CXlLTVDD';
  DisplayValues = [false, false];
  day: any;
  SendData(){

  }
  Upvote(){
    console.log(this.downvoteActive);
    var UpVotes;
    if(this.downvoteActive == true){
      alert('You have currently downvoted this employee');
    }
    else{
        if(this.upvoteActive == false){
          this.upvoteActive = true;
          //UpVotes = this.upvotes + 1;
          //this._empService.Upvote(this.employeeKey, UpVotes, this.downvotes);
        }
        else{
          this.upvoteActive = false;
          //UpVotes = this.upvotes - 1;
          //this._empService.Upvote(this.employeeKey, UpVotes, this.downvotes);
        }
        console.log(this.upvoteActive);
    }
  }

  Downvote(){
    var DownVotes;
    console.log(this.upvoteActive);
    if(this.upvoteActive == true){
      alert('You have currently upvoted this employee');
    }
    else{
        if(this.downvoteActive == false){
          this.downvoteActive = true;
          //DownVotes = this.downvotes + 1;
          //this._empService.Upvote(this.employeeKey,this.upvotes, DownVotes);
        }
        else{
          this.downvoteActive = false;
          //DownVotes = this.downvotes - 1;
          //this._empService.Upvote(this.employeeKey,this.upvotes, DownVotes);
        }
        console.log(this.downvoteActive);
    }
  }

  canSave: boolean = true;
  isUnchanged: boolean = true;

  setClasses() {
    let classes =  {
      saveable: this.canSave,      // true
      modified: !this.isUnchanged, // false
    };
    return classes;
  }
  UpdateAppreciation(){
    console.log(this.comment)
    this._empService.Comment(this.employeeKey, this.comment);
    this.comment = null;
  }

  constructor(private router: Router,
              public _empService: EmployeesService) { 
              
              }

  ngOnInit(){
  }

}
