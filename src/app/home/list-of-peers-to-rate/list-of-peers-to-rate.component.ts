import { Component, OnInit, ElementRef, Output, EventEmitter, Inject } from '@angular/core';
import { EmployeesService } from '../../services/employees.service';
import { RatingsService } from './../../services/ratings.service';
import { MainFooterComponent } from './../main-footer/main-footer.component';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFire, AuthProviders } from 'angularfire2';


@Component({
  selector: 'list-of-peers-to-rate',
  templateUrl: './list-of-peers-to-rate.component.html',
  styleUrls: ['./list-of-peers-to-rate.component.scss'],
  providers: [EmployeesService, RatingsService]
})
export class ListOfPeersToRateComponent implements OnInit {
  name: any;
  role: any;
  project: any;
  rating: any;
  employees: any;
  projects: any;
  savedRatings=[];
  getRating: any;
  UserEmail: any;
  ratingValue: any;
  trial : any;
  hey: any;
  csdLogin: any = false;
  constructor(private _employeeService: EmployeesService,
              private _ratingsService: RatingsService,
              private elementRef: ElementRef,
              public angularFire: AngularFire,
              private router: Router) {

              angularFire.auth.subscribe(user => {
                if(!user) {
                  // user logged out
                  this.router.navigate(["home"]);
                }
                else{
                  this.UserEmail = user.auth.email;
                }
              });
              }

  logout() {
    this.angularFire.auth.logout();
  }

  ngOnInit() {
    this.getEmployees();
  }

  getEmployees(){
    this._employeeService.getEmployees(this.UserEmail)
    .subscribe(data => {
                        this.employees = (data[0]);
                        this.projects = (data[1]);
                        this.savedRatings = (data[2]);
                        this.csdLogin = (data[3][0]);
                      });
  }
/******************** IN-COMPONENT DATA RETRIEVING BLOCK ***********************
  getRatings(){
    this._ratingsService.getRatings()
    .subscribe(data => {
                         this.getRating = data;
                         this.getSavedRatings();},

    );
  }
  getSavedRatings(){
    for(var i=0; i<this.employees.length; i++){
      this.savedRatings[i] = 0;
      for(var j=0; j<this.getRating.length; j++){
         if((this.getRating[j].from_email_id == this.UserEmail)
             && (this.getRating[j].email_id == this.employees[i].email_id)
             && (this.getRating[j].project_id == this.projects[i])){
             this.savedRatings[i] = this.getRating[j].rating;
         }
      }
    }
  }
****************************(16/12/2016: friday)**********************************/

}
