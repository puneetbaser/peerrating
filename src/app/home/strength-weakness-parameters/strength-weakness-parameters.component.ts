import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'strength-weakness-parameters',
  templateUrl: './strength-weakness-parameters.component.html',
  styleUrls: ['./strength-weakness-parameters.component.scss']
})
export class StrengthWeaknessParametersComponent implements OnInit {
  display1 = false;
  display2 = true;

  @Output() Strength = new EventEmitter<string>() ;
  @Output() Weakness = new EventEmitter<string>() ;

  ShowStrengths(){
    this.display1 = false;
    this.display2 = true;
  }

  ShowWeakness(){
    this.display1 = true;
    this.display2 = false;
  }

  //Updating Strengths array
  updateStrength(x){
    var strength = document.getElementsByClassName(x)[0].innerHTML;
    this.Strength.emit(strength);
    if(document.getElementsByClassName(x)[1].classList.contains('mr-quality-active')){
      alert('Rated as Weakness');
    }
    else{
        if(document.getElementsByClassName(x)[0].classList.contains('mr-quality-active')){
          document.getElementsByClassName(x)[0].classList.remove("mr-quality-active");
        }
        else{
          document.getElementsByClassName(x)[0].classList.add("mr-quality-active");
        }
    }
  }

  //Updating Weakness array
  updateWeakness(x){
    var weakness = document.getElementsByClassName(x)[1].innerHTML;
    this.Weakness.emit(weakness);
    if(document.getElementsByClassName(x)[0].classList.contains('mr-quality-active')){
      alert('Rated as Strength');
    }
    else{
        if(document.getElementsByClassName(x)[1].classList.contains('mr-quality-active')){
          document.getElementsByClassName(x)[1].classList.remove("mr-quality-active");
        }
        else{
          document.getElementsByClassName(x)[1].classList.add("mr-quality-active");
        }
    }
  }

  constructor() { }

  ngOnInit() {}

}
