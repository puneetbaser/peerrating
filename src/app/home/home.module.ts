import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AngularFireModule } from 'angularfire2';


import { HomeRoutingModule } from './home-routing.module';
import { ListOfPeersToRateComponent } from './list-of-peers-to-rate/list-of-peers-to-rate.component'
import { PeerRatingTileComponent } from './peer-rating-tile/peer-rating-tile.component'
import { HomeComponent } from './home.component';
import { RateYourPeerComponent } from './rate-your-peer/rate-your-peer.component';
import { StrengthWeaknessParametersComponent } from './strength-weakness-parameters/strength-weakness-parameters.component';
import { MainFooterComponent } from './main-footer/main-footer.component';

export const firebaseConfig = {
  apiKey: "AIzaSyCy0w1mXREQRhV74scNNJNuQ0byu5YdcA4",
  authDomain: "peerratingapp.firebaseapp.com",
  databaseURL: "https://peerratingapp.firebaseio.com",
  storageBucket: "peerratingapp.appspot.com",
  messagingSenderId: "668419528864"
};

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    HomeRoutingModule,
    AngularFireModule.initializeApp(firebaseConfig)
    ],
  declarations: [
    ListOfPeersToRateComponent,
    MainFooterComponent,
    PeerRatingTileComponent,
    HomeComponent,
    RateYourPeerComponent,
    StrengthWeaknessParametersComponent,
  ],
  providers: []
})
export class HomeModule {}
