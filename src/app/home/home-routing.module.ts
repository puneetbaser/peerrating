import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListOfPeersToRateComponent } from './list-of-peers-to-rate/list-of-peers-to-rate.component'
import { RateYourPeerComponent } from './rate-your-peer/rate-your-peer.component'


/** Custom components **/

const routes : Routes = [
  {
    path: '',
    redirectTo: '/list-of-peers-to-rate',
    pathMatch: 'full'
  },
  {
    path: 'list-of-peers-to-rate',
    component: ListOfPeersToRateComponent
  },
  {
    path: 'rate-your-peer',
    component: RateYourPeerComponent
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
})

export class HomeRoutingModule{}
