import { Component, OnInit, ElementRef } from '@angular/core';
import { StrengthWeaknessParametersComponent } from '../strength-weakness-parameters/strength-weakness-parameters.component';
import { PeerRatingTileComponent } from './../peer-rating-tile/peer-rating-tile.component';
import { MainFooterComponent } from './../../main-footer/main-footer.component';
import { RatingsService } from './../../services/ratings.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFire, AuthProviders } from 'angularfire2';

@Component({
  selector: 'rate-your-peer',
  templateUrl: './rate-your-peer.component.html',
  styleUrls: ['./rate-your-peer.component.scss'],
  providers: [RatingsService]
})
export class RateYourPeerComponent implements OnInit {

  Ratings: any = 0;
  Strengths: any = [];
  Weakness : any = [];
  EmployeeName: any;
  EmployeeRole: any;
  EmployeeProject: any;
  EmployeeEmail: any;
  UpdatedRatings: any;
  ImageSrc = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ3tpHxLZOkETrMCgUWwY6_u3WEUbk5nsiHoU1d29-1CXlLTVDD';
  Months = ['January', 'February', 'March','April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  date: any;
  month: any;
  year: any;
  validInput: any = false;
  updateStrength(x){
    if(!(this.Strengths.includes(x))){
        this.Strengths.push(x);
        //console.log(this.Strengths);
      }
    else{
      var index = this.Strengths.indexOf(x);
      this.Strengths.splice(index, 1);
      //console.log(this.Strengths);
    }
  }
  SendRating(x){
    this.Ratings = x;
  }
  updateWeakness(x){
    if(!(this.Weakness.includes(x))){
        this.Weakness.push(x);
        //console.log(this.Weakness);
      }
    else{
      var index = this.Weakness.indexOf(x);
      this.Weakness.splice(index, 1);
      //console.log(this.Weakness);
    }
  }

  constructor(private _ratingsService: RatingsService,
              private elementRef: ElementRef,
              private route: ActivatedRoute,
              private router: Router,
              public angularFire: AngularFire) {
          this.date = new Date();
          this.year = this.date.getFullYear();
          if((this.date.getMonth()) == 0){
            this.month = this.Months[11];
            this.year = this.year - 1;
          }
          else{
            this.month = this.Months[this.date.getMonth()];
          }
          this.route.queryParams.subscribe( params => {
                this.EmployeeName = params["EmployeeName"];
                this.EmployeeRole = params["EmployeeRole"];
                this.EmployeeProject = params["EmployeeProject"];
                this.EmployeeEmail = params["EmployeeEmail"];

          });

   }

  ListOfPeers(){
    if(this.validInput == true){
      this.router.navigate(["list-of-peers-to-rate"]);
    }
  }

  _ratingDetails;
  updateRating(): void{
    if(this.Ratings == 0){
      alert('Please give your ratings');
    }
    if((this.Strengths.length ==0)&&(this.Weakness.length ==0)){
      alert('Select atleast one Strength or Weakness');
    }
    if((this.Ratings>0) && ((this.Strengths.length != 0) || (this.Weakness.length != 0))){
        this.validInput = true;
        this.angularFire.auth.subscribe(user =>{
          if(user){
            this._ratingDetails = {
                  "email_id": this.EmployeeEmail,
                  "year": this.year,
                  "month": this.month,
                  "project_id": this.EmployeeProject,
                  "from_email_id": user.auth.email,
                  "rating": this.Ratings,
                  "strength": this.Strengths,
                  "weakness": this.Weakness
              };
              this._ratingsService.updateRating(this._ratingDetails);
          }

          }
        );
    }
  }
  ngOnInit(){}

}
