import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'peer-rating-tile',
  templateUrl: './peer-rating-tile.component.html',
  styleUrls: ['./peer-rating-tile.component.scss']
})
export class PeerRatingTileComponent implements OnInit {
  @Input() EmployeeName: any;
  @Input() EmployeeRole: any;
  @Input() EmployeeProject: any;
  @Input() EmployeeEmail: any;
  @Input() SavedRating: number = 5;

  ImageSrc = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ3tpHxLZOkETrMCgUWwY6_u3WEUbk5nsiHoU1d29-1CXlLTVDD';
  DisplayValues = [false, false, false, false, false];

  /*SendRating(x){
    this.Rating.emit(x);
  }*/
  yes0 = false;
  yes1 = false;
  yes2 = false;
  yes3 = false;
  yes4 = false;
  day: any;
  SendData(){

  }
  StarDisplay(x){
    var i =0;
    for(i=0; i<x; i++){
      this.DisplayValues[i] = true;
    }
    this.yes0 = this.DisplayValues[0];
    this.yes1 = this.DisplayValues[1];
    this.yes2 = this.DisplayValues[2];
    this.yes3 = this.DisplayValues[3];
    this.yes4 = this.DisplayValues[4];
  }
  canSave: boolean = true;
  isUnchanged: boolean = true;

  setClasses() {
    let classes =  {
      saveable: this.canSave,      // true
      modified: !this.isUnchanged, // false
    };
    return classes;
  }

  constructor(private router: Router) { }

  RateYourPeer(){
    this.day = new Date().getDate();
    if (this.SavedRating == 0){
          if ((this.day > 0) && (this.day < 31)){
              let navigationExtras : NavigationExtras = {
                    queryParams: {
                         "EmployeeName": this.EmployeeName,
                         "EmployeeRole": this.EmployeeRole,
                         "EmployeeProject": this.EmployeeProject,
                         "EmployeeEmail": this.EmployeeEmail
                    }
              }
              this.router.navigate( ["rate-your-peer"], navigationExtras);
          }
          else{
              if(this.day > 31){
                alert("You can rate from 21st");
              }
              else{
                alert("Rating period is over");
              }
          }
   }
  else{
    alert("You have already rated this peer");
    }
  }


  ngOnInit(){
    this.StarDisplay(this.SavedRating);
  }

}
