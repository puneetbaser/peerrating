"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var home_routing_module_1 = require("./home-routing.module");
var list_of_peers_to_rate_component_1 = require("./list-of-peers-to-rate/list-of-peers-to-rate.component");
var peer_rating_tile_component_1 = require("./peer-rating-tile/peer-rating-tile.component");
var home_component_1 = require("./home.component");
var rate_your_peer_component_1 = require("./rate-your-peer/rate-your-peer.component");
var strength_weakness_parameters_component_1 = require("./strength-weakness-parameters/strength-weakness-parameters.component");
var HomeModule = (function () {
    function HomeModule() {
    }
    return HomeModule;
}());
HomeModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            platform_browser_1.BrowserModule,
            forms_1.FormsModule,
            http_1.HttpModule,
            home_routing_module_1.HomeRoutingModule
        ],
        declarations: [
            list_of_peers_to_rate_component_1.ListOfPeersToRateComponent,
            peer_rating_tile_component_1.PeerRatingTileComponent,
            home_component_1.HomeComponent,
            rate_your_peer_component_1.RateYourPeerComponent,
            strength_weakness_parameters_component_1.StrengthWeaknessParametersComponent,
        ],
        providers: []
    }),
    __metadata("design:paramtypes", [])
], HomeModule);
exports.HomeModule = HomeModule;
